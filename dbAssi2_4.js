const express = require('express');
const app = express();

const mysql = require('mysql2');
app.use(express.static("sf"));

let dbparams =
{
    host: 'localhost',
    user: 'root',
    password: 'cdac',
    database: 'Assignments',
    port: 3306
};

const con = mysql.createConnection(dbparams);
app.get("/Location", (req, resp) => {
    let pin = req.query.pin;
    console.log("Fetching from database");

    let loc = { status: false, place: [] };

    con.query('select areaname from location_tbl where pin=?', [pin],
        (error, row) => {
            if (error) {
                console.log("Error has occurred " + error);
            }
            else if (row.length > 0) {
                console.log("Inside query else if block ");
                loc.status = true;
                loc.place = row;
                console.log(row);
            }
            resp.send(loc);
        });

});
app.listen(900, function () {
    console.log("Server listening at port 900...");
});
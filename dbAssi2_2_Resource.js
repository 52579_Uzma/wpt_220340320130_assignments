const express = require('express');
const app = express();

const mysql = require('mysql2'); 

app.use(express.static("sf"));

app.listen(500, ()=>{
    console.log("Server running at port 500");
});
app.get('/getResourceDetails', (req, resp) => {
    console.log("Inside get  -> getResourceDetails");
    console.log("ajax function called , connect with the server ");
    const dataBaseObject = {
        host: 'localhost',
        user: 'root',
        password: 'cdac',
        database: 'Assignments',
        port: 3306
    }
    const conn = mysql.createConnection(dataBaseObject);

    let output = { checkStatus: false, detail: { resourceId: 0, resourceName: "" ,status :false } }
    let resourceId = req.query.resourceId;
    console.log(resourceId);
    conn.query('select resourceId, resourceName , status from resource where resourceId = ?', [resourceId],
        (error, rows) => {
            if (error) {
                console.log(error);
            }
            else {
                if (rows.length > 0) {
                    output.checkStatus = true;
                    output.detail = rows[0];
                    console.log("Inside else if block ");
                }
                else {
                    console.log("Resource id is not found");
                }
            }
            console.log(output);
            resp.send(output);
        });

});
app.get('/insertResourceDetails', (req, resp) => {
    console.log("Inside get  -> insertResourceDetails");
    console.log("ajax function called , connect with the server ");
    const dataBaseObject = {
        host: 'localhost',
        user: 'root',
        password: 'cdac',
        database: 'Assignments',
        port: 3306
    }
    const conn = mysql.createConnection(dataBaseObject);

    
    let resDetails = {resourceId: req.query.resourceId, resourceName: req.query.resourceName, status: req.query.status}
    let output = {checkStatus: false}

    
    conn.query('insert into resource values(?,?,?)',
     [resDetails.resourceId ,resDetails.resourceName , resDetails.status],
     (error, res) => {
        if (error) {
            console.log(error);
        }
        else 
        {
            if (res.affectedRows > 0) {
                output.checkStatus = true;
            }
            else {
                console.log("Insert operation failed");
            }
        }
     
            console.log(output);
            resp.send(output);

        });

});

    app.get('/updateResourceDetails', (req, resp) => {
        console.log("Inside get  -> updateResourceDetails");
        console.log("ajax function called");
        const dbobject = {
            host: 'localhost',
            user: 'root',
            password: 'cdac',
            database: 'Assignments',
            port: 3306
        }
        const conn = mysql.createConnection(dbobject);

        let output = { checkStatus: false }
        let resourceId = req.query.resourceId;
        let resourceName = req.query.resourceName;
        let status = req.query.status ;
        
        conn.query('update resource set resourceName = ? ,status=? where resourceId = ?', [resourceName, status, resourceId],
            (error, res) => {
                if (error) {
                    console.log(error);
                }
                else {
                    if (res.affectedRows > 0) {
                        output.checkStatus = true;
                    }
                    else {
                        console.log("Did not update");
                    }
                }
                console.log(output);
                resp.send(output);
            });
    });


    
// Single select

app.get('/SingleSelect' , (req, resp) => {
    console.log("Inside get  -> updateResourceDetails");
    console.log("ajax function called");
    const dbobject = {
        host: 'localhost',
        user: 'root',
        password: 'cdac',
        database: 'Assignments',
        port: 3306
    }
    const conn = mysql.createConnection(dbobject);
    let output = {checkStatus : false};
    let resourceId = req.query.resourceId ;
    conn.query("select resourceId , resourceName , status from resource where resourceid=?", [resourceId], (err, rows) => {

        if (err) {
            console.log("Erroe while fetching data");
        } 
        else {
            if (rows.length > 0) {
                output.checkStatus = true ;
                    console.log("Succeed: " + rows[0].resourceId + " " + rows[0].resourceName + " " + rows[0].status);
                } 
                else {
                    console.log("None rows are shown");
                }
        }   
    
        
        //resp.send(output);

    });

});



// multiselect
app.get('/MultipleSelect' , (req, resp) => {
        console.log("Inside get  -> updateResourceDetails");
        console.log("ajax function called");
        const dbobject = {
            host: 'localhost',
            user: 'root',
            password: 'cdac',
            database: 'Assignments',
            port: 3306
        }
        const conn = mysql.createConnection(dbobject);
        let status = false;
        conn.query("select resourceId , resourceName,status from resource where status=?",
            [status], (err, rows) => {
                if (err) {
                    console.log("Error occure");
                } else {
                    if (rows.length > 0) {
                        for (let i = 0; i < rows.length; i++) {
                            console.log(rows[i].resourceId + " " + rows[i].resourceName + " " + rows[i].status);
                        }
                    } else {
                        console.log("No data found");
                    }

                }
    });
});    
